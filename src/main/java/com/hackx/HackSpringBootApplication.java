package com.hackx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HackSpringBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(HackSpringBootApplication.class, args);
    }
}
